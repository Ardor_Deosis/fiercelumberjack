#include <cstdlib>
#include <cstddef>
#include <cmath>
#include <iostream>
#include <ios>
#include <vector>

#include <OpenSG/OSGGLUT.h>
#include <OpenSG/OSGConfig.h>
#include <OpenSG/OSGSimpleGeometry.h>
#include <OpenSG/OSGGLUTWindow.h>
#include <OpenSG/OSGMultiDisplayWindow.h>
#include <OpenSG/OSGSceneFileHandler.h>

#include <OSGCSM/OSGCAVESceneManager.h>
#include <OSGCSM/OSGCAVEConfig.h>
#include <OSGCSM/appctrl.h>

#include <vrpn_Tracker.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>


#include <OpenSG/OSGMaterialGroup.h>
#include <OpenSG/OSGImage.h>
#include <OpenSG/OSGSimpleTexturedMaterial.h>
#include <OpenSG/OSGSceneFileHandler.h>
#include <OpenSG/OSGSwitch.h>

#include "AABBCollision.h"
#include "MaterialStack.h"

extern OSGCSM::CAVESceneManager *mgr;

extern AABBCollision cd;


extern float ballSize;
extern ComponentTransformRecPtr ballTrans;

extern ComponentTransformRecPtr testBallTrans;


// ball trail renderer
int trailLength = 20;
std::vector<Vec3f> ballTrail;
std::vector<ComponentTransformRecPtr> ballTrailTrans;

// movement ray
std::vector<Vec3f> movementRayPoints;
std::vector<GeometryRecPtr> movementRayBallsGeom;
std::vector<ComponentTransformRecPtr> movementRayBallsTrans;
std::vector<SwitchRecPtr> movementRayBallsSwitch;

NodeRecPtr movementRayGround;
ComponentTransformRecPtr movementRayGroundTrans;

int movementRaySteps = 0;
bool movementRayVisible = false;
bool movementValid = false;
Vec3f movementPosition;

Vec3f movRayPos = Vec3f(0, 150, 100);
Vec3f movRayDir = Vec3f(1, 1, 1);



extern Vec3f wand_world_position;
extern Vec3f wand_world_speed;
extern Quaternion wand_world_orientation;


// extern functions
NodeTransitPtr makeColBox(Real32 x, Real32 y, Real32 z, Real32 xs, Real32 ys, Real32 zs, Material* mat = nullptr, const AABBCollision::ColliderData &colData = AABBCollision::ColliderData());


NodeTransitPtr buildScene()
{
	NodeTransitPtr root = Node::create();
	root->setCore(Group::create());

	cd.clear();



	for (int x = -10; x < 30; ++x) {
		for (int y = -20; y < 20; ++y) {
			for (int z = -30; z < 10; ++z) {
				bool setBox = (
					(x > -6 && x < 6 && y == 0 && z < 3 && z > -20) || // r1 and r2 floor
					(x > -6 && x < 6 && y == 10 && z < 3 && z > -20) || // r1 and r2 ceiling
					(x > -6 && x < 6 && y > 0 && y < 10 && z == 3) || // w1
					(x == -6 && y > 0 && y < 10 && z < 3 && z > -20) || //w2
					(x == 6 && y > 0 && y < 10 && z < 3 && z > -8) || //w3
					(x > -6 && x < 19 && y > 0 && y < 10 && z == -20) || //w4
					(x > -6 && x < -2 && y > 0 && y < 10 && z == -8) || //w5
					(x > 0 && x < 19 && y > 0 && y < 10 && z == -8) || //w6
					(x == 19 && y > 4 && y < 10 && z < -8 && z > -20) || //w7
					((x > 7 && x < 19 && y == 4 && z < -8 && z > -20) && !(x > 11 && x < 15 && z < -12 && z > -16)) || // r3 floor
					(x > 5 && x < 19 && y == 10 && z < -8 && z > -20) || // r3 ceiling
					(x > 3 && x < 8 && y == 1 && z < -8 && z > -20) || //step1
					(x > 3 && x < 8 && y == 2 && z < -8 && z > -16) || //step2
					(x > 3 && x < 8 && y == 3 && z < -8 && z > -12) || //step3
					(x == 8 && y > 1 && y < 5 && z < -11 && z > -20) || //small wall
					(x > 11 && x < 15 && y == 3 && z < -12 && z > -16) || // indent floor
					(x == 13 && y > 3 && y < 10 && y != 7 && z == -14) || // column
					(x > 1 && x < 4 && y > 0 && y < 3 && z > -6 && z < -3) // box1
					);
				if (setBox) {
					root->addChild(makeColBox((x * 100), (y * 100) - 50, (z * 100), 100, 100, 100, MaterialStack::Floor()));
				}
				if (x == 13 && y == 7 && z == -14) {
					AABBCollision::ColliderData win_data;
					win_data["win_trigger"] = NULL;

					root->addChild(makeColBox((x * 100), (y * 100) - 50, (z * 100), 100, 100, 100, MaterialStack::WinCube(), win_data));
				}
			}
		}
	}




	cd.finalize();



	NodeRecPtr ball = makeSphere(2, ballSize);
	ballTrans = ComponentTransform::create();


	NodeRecPtr ballTransNode = Node::create();
	ballTransNode->setCore(ballTrans);
	ballTransNode->addChild(ball);

	root->addChild(ballTransNode);

	NodeRecPtr testBall = makeSphere(2, 5);
	testBallTrans = ComponentTransform::create();
	testBallTrans->setTranslation(Vec3f(0, 150, 100));


	NodeRecPtr testBallTransNode = Node::create();
	testBallTransNode->setCore(testBallTrans);
	testBallTransNode->addChild(testBall);

	root->addChild(testBallTransNode);

	// ball material
	GeometryRecPtr ballGeoCore = dynamic_cast<Geometry*>(ball->getCore());
	ballGeoCore->setMaterial(MaterialStack::Ball());


	// init trail renderer
	// balls
	ballTrail.resize(trailLength);
	ballTrailTrans.resize(trailLength);


	for (int i = 0; i < trailLength; ++i) {
		ballTrail[i] = ballTrans->getTranslation();

		ballTrailTrans[i] = ComponentTransform::create();
		ballTrailTrans[i]->setTranslation(ballTrans->getTranslation());

		NodeRecPtr ballTrailTransNode = Node::create();
		ballTrailTransNode->setCore(ballTrailTrans[i]);
		NodeRecPtr ballTrail = makeSphere(2, 4.0f + 6.0f * (trailLength - i) / trailLength);
		ballTrailTransNode->addChild(ballTrail);

		root->addChild(ballTrailTransNode);

		// material
		SimpleMaterialRecPtr m_trail = SimpleMaterial::create();
		m_trail->setDiffuse(Color3f(1,0.8f,0));
		m_trail->setAmbient(Color3f(0.8f, 0.2f, 0.2f));
		if (i == 0) {
			m_trail->setTransparency(1.0f);
		}
		m_trail->setShininess(3.0f);
		m_trail->setTransparency(0.9f + 0.1f * (1.0f - std::pow((trailLength - i) / (float)trailLength, 2)));

		GeometryRecPtr trailGeoCore = dynamic_cast<Geometry*>(ballTrail->getCore());
		trailGeoCore->setMaterial(m_trail);

	}


	// init movement ray
	movementRayPoints.resize(128);
	movementRayBallsGeom.resize(128);
	movementRayBallsTrans.resize(128);
	movementRayBallsSwitch.resize(128);
	for (int i = 0; i < movementRayBallsGeom.size() - 1; ++i) {
		movementRayBallsTrans[i] = ComponentTransform::create();
		movementRayBallsTrans[i]->setTranslation(ballTrans->getTranslation());

		NodeRecPtr ballNode = Node::create();
		ballNode->setCore(movementRayBallsTrans[i]);

		NodeRecPtr movRayGeo = makeSphere(2, 1);
		ballNode->addChild(movRayGeo);
		movementRayBallsGeom[i] = dynamic_cast<Geometry*>(movRayGeo->getCore());


		movementRayBallsSwitch[i] = Switch::create();

		NodeRecPtr switchNode = Node::create();
		switchNode->setCore(movementRayBallsSwitch[i]);
		switchNode->addChild(ballNode);

		root->addChild(switchNode);

		movementRayBallsSwitch[i]->setChoice(1);
	}

	movementRayGroundTrans = ComponentTransform::create();

	movementRayGround = Node::create();
	movementRayGround->setCore(movementRayGroundTrans);

	root->addChild(movementRayGround);


	return root;
	//root->addChild(
	// you will see a donut at the floor, slightly skewed, depending on head_position
	//return makeTorus(10.f, 50.f, 32.f, 64.f);
}




void updateBallTrail () {
	for (int i = ballTrail.size() - 1; i > 0; --i) {
		ballTrail[i] = ballTrail[i-1];
		ballTrailTrans[i]->setTranslation(ballTrail[i]);
	}
	ballTrail[0] = ballTrans->getTranslation();
	ballTrailTrans[0]->setTranslation(ballTrail[0]);
}

void updateMovementRay () {
	Vec3f pos = movRayPos;
	Vec3f dir = movRayDir;

	pos = wand_world_position;
	wand_world_orientation.multVec(Vec3f(0, 0, -1), dir);


	dir.normalize();
	bool foundHit = false;
	AABBCollision::CollisionAxis axis;
	movementRayGround->clearChildren();
	for (int i = 0; i < movementRayPoints.size() - 1; ++i) {

		movementRayPoints[i] = pos;
		movementRayBallsSwitch[i]->setChoice(1);

		if (!foundHit) {
			axis = cd.raycast(movementRayPoints[i-1], pos, &movementPosition);
			if (i > 0 && axis) {
				movementRayBallsTrans[i]->setTranslation(movementPosition);
				movementRaySteps = i;
				foundHit = true;
			} else {
				movementRayBallsTrans[i]->setTranslation(pos);
			}
		}
		pos += dir * (5.0f + i * 0.5f);

		dir += Vec3f(0,-0.05f,0);
		dir.normalize();
	}


	Vec3f placeholder;
	movementValid = (axis == AABBCollision::CollisionAxisYBwd &&
		cd.raycast(movementPosition + Vec3f(0,1,0), movementPosition + Vec3f(100,1,0), &placeholder) == AABBCollision::CollisionAxisNone &&
		cd.raycast(movementPosition + Vec3f(0,1,0), movementPosition + Vec3f(-100,1,0), &placeholder) == AABBCollision::CollisionAxisNone &&
		cd.raycast(movementPosition + Vec3f(0,1,0), movementPosition + Vec3f(0,1,100), &placeholder) == AABBCollision::CollisionAxisNone &&
		cd.raycast(movementPosition + Vec3f(0,1,0), movementPosition + Vec3f(0,1,-100), &placeholder) == AABBCollision::CollisionAxisNone);
	for (int i = 0; i <= movementRaySteps; ++i) {
		movementRayBallsSwitch[i]->setChoice(0);

		if (movementValid) {
			movementRayBallsGeom[i]->setMaterial(MaterialStack::MovRayGreen());
		} else {
			movementRayBallsGeom[i]->setMaterial(MaterialStack::MovRayRed());
		}
	}
	if (axis == AABBCollision::CollisionAxisYBwd) {
		GeometryRecPtr movementRayGroundGeoCore = makeBoxGeo(200, 1, 200, 1, 1, 1);
		NodeRecPtr movementRayGroundGeo = Node::create();
		movementRayGroundGeo->setCore(movementRayGroundGeoCore);
		movementRayGroundTrans->setTranslation(movementPosition);
		movementRayGround->addChild(movementRayGroundGeo);
		if (movementValid) {
			movementRayGroundGeoCore->setMaterial(MaterialStack::MovRayGreen());
		} else {
			movementRayGroundGeoCore->setMaterial(MaterialStack::MovRayRed());
		}
	}
	//movementRayVisible = false;
}