#include <cstdlib>
#include <cstddef>
#include <cmath>
#include <iostream>
#include <ios>
#include <vector>

#include <OpenSG/OSGGLUT.h>
#include <OpenSG/OSGConfig.h>
#include <OpenSG/OSGSimpleGeometry.h>
#include <OpenSG/OSGGLUTWindow.h>
#include <OpenSG/OSGMultiDisplayWindow.h>
#include <OpenSG/OSGSceneFileHandler.h>

#include <OSGCSM/OSGCAVESceneManager.h>
#include <OSGCSM/OSGCAVEConfig.h>
#include <OSGCSM/appctrl.h>

#include <vrpn_Tracker.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>


#include <OpenSG/OSGMaterialGroup.h>
#include <OpenSG/OSGImage.h>
#include <OpenSG/OSGSimpleTexturedMaterial.h>
#include <OpenSG/OSGSceneFileHandler.h>
#include <OpenSG/OSGSwitch.h>

#include "AABBCollision.h"
#include "MaterialStack.h"



OSG_USING_NAMESPACE

	OSGCSM::CAVEConfig cfg;
OSGCSM::CAVESceneManager *mgr = nullptr;
vrpn_Tracker_Remote* tracker =  nullptr;
vrpn_Button_Remote* button = nullptr;
vrpn_Analog_Remote* analog = nullptr;

extern const unsigned int PHYSICS_TIMESTEP_LENGTH; // ms
static const float MOVEMENT_VALUE_SCALE = 0.02f;

extern AABBCollision cd;

// test test test
extern float ballSize;

ComponentTransformRecPtr ballTrans;
extern bool ballGrabbed;
extern bool ballGrabActive;
extern float BallHitSpeedScale;

ComponentTransformRecPtr testBallTrans;


// ball trail renderer
//int trailLength = 20;
//std::vector<Vec3f> ballTrail;
//std::vector<ComponentTransformRecPtr> ballTrailTrans;




// movement ray
extern std::vector<Vec3f> movementRayPoints;
//std::vector<GeometryRecPtr> movementRayBallsGeom;
//std::vector<ComponentTransformRecPtr> movementRayBallsTrans;
extern std::vector<SwitchRecPtr> movementRayBallsSwitch;

extern NodeRecPtr movementRayGround;
//ComponentTransformRecPtr movementRayGroundTrans;

//int movementRaySteps = 0;
extern bool movementRayVisible;
extern bool movementValid;
extern Vec3f movementPosition;




Vec3f wand_world_position;
Vec3f wand_world_speed;
Quaternion wand_world_orientation;



void cleanup()
{
	delete mgr;
	delete tracker;
	delete button;
	delete analog;
}



// functions
void print_tracker();
void updateBallTrail ();
void updateMovementRay ();

void updatePhysics();
void physicsDebug(Vec3f m);

void reset_scene();
void update_wand_speed(Vec3f newPos);


// extern functions
NodeTransitPtr buildScene();


NodeTransitPtr makeColBox(Real32 x, Real32 y, Real32 z, Real32 xs, Real32 ys, Real32 zs, Material* mat = nullptr, const AABBCollision::ColliderData &colData = AABBCollision::ColliderData())
{
	// could reuse the box geo and just change transform
	GeometryRecPtr boxGeo = makeBoxGeo(xs, ys, zs, 1, 1, 1);
	if (!mat) {
		mat = MaterialStack::Debug();
	}
	boxGeo->setMaterial(mat);

	ComponentTransformRecPtr boxTrans = ComponentTransform::create();
	boxTrans->setTranslation(Vec3f(x, y, z));

	NodeTransitPtr boxTransNode = Node::create();
	boxTransNode->setCore(boxTrans);

	NodeRecPtr boxGeoNode = Node::create();
	boxGeoNode->setCore(boxGeo);

	boxTransNode->addChild(boxGeoNode);

	cd.addBox(BoxVolume(x - xs / 2, y - ys / 2, z - zs / 2, x + xs / 2, y + ys / 2, z + zs / 2), colData);

	return boxTransNode;
}


template<typename T>
T scale_tracker2cm(const T& value)
{
	static const float scale = OSGCSM::convert_length(cfg.getUnits(), 1.f, OSGCSM::CAVEConfig::CAVEUnitCentimeters);
	return value * scale;
}

auto head_orientation = Quaternion(Vec3f(0.f, 1.f, 0.f), 3.141f);
auto head_position = Vec3f(0.f, 170.f, 200.f);	// a 1.7m Person 2m in front of the scene

void VRPN_CALLBACK callback_head_tracker(void* userData, const vrpn_TRACKERCB tracker)
{
	head_orientation = Quaternion(tracker.quat[0], tracker.quat[1], tracker.quat[2], tracker.quat[3]);
	head_position = Vec3f(scale_tracker2cm(Vec3d(tracker.pos)));
}

auto wand_orientation = Quaternion();
auto wand_position = Vec3f();


extern Vec3f wand_speed;	// units per second


void VRPN_CALLBACK callback_wand_tracker(void* userData, const vrpn_TRACKERCB tracker)
{

	wand_orientation = Quaternion(tracker.quat[0], tracker.quat[1], tracker.quat[2], tracker.quat[3]);
	wand_position = Vec3f(scale_tracker2cm(Vec3d(tracker.pos)));

	update_wand_speed(wand_position);
}

auto analog_values = Vec3f();
void VRPN_CALLBACK callback_analog(void* userData, const vrpn_ANALOGCB analog)
{
	if (analog.num_channel >= 2)
		analog_values = Vec3f(analog.channel[0], 0, -analog.channel[1]);
}

void VRPN_CALLBACK callback_button(void* userData, const vrpn_BUTTONCB button)
{
	if (/*button.button == 0 && */ button.state == 1) {
		print_tracker();
		//std::cout << "button: " << button.button << "\n";
	}

	if (button.button == 3 && button.state == 1) {
		movementRayVisible = true;
	} else if (button.button == 3 && button.state == 0) {

		// release ball, don't want player to teleport around with it :)
		ballGrabbed = false;

		if (movementValid)
		{
			mgr->setTranslation(movementPosition);
		}

		movementRayVisible = false;
		movementValid = false;

		movementRayGround->clearChildren();
		for (int i = 0; i < movementRayPoints.size() - 1; ++i) {
			movementRayBallsSwitch[i]->setChoice(1);
		}
	}

	if (button.button == 0)
	{
		ballGrabActive = (button.state == 1);
	}

	if (button.button == 1)
	{
		reset_scene();
	}
}

void InitTracker(OSGCSM::CAVEConfig &cfg)
{
	try
	{
		const char* const vrpn_name = "DTrack@localhost";
		tracker = new vrpn_Tracker_Remote(vrpn_name);
		tracker->shutup = true;
		tracker->register_change_handler(NULL, callback_head_tracker, cfg.getSensorIDHead());
		tracker->register_change_handler(NULL, callback_wand_tracker, cfg.getSensorIDController());
		button = new vrpn_Button_Remote(vrpn_name);
		button->shutup = true;
		button->register_change_handler(nullptr, callback_button);
		analog = new vrpn_Analog_Remote(vrpn_name);
		analog->shutup = true;
		analog->register_change_handler(NULL, callback_analog);
	}
	catch(const std::exception& e) 
	{
		std::cout << "ERROR: " << e.what() << '\n';
		return;
	}
}

void check_tracker()
{
	tracker->mainloop();
	button->mainloop();
	analog->mainloop();
}

void print_tracker()
{
	std::cout << "Head position: " << head_position << " orientation: " << head_orientation << '\n';
	std::cout << "Wand position: " << wand_position << " orientation: " << wand_orientation << '\n';
	std::cout << "Analog: " << analog_values << '\n';
}


void keyboard(unsigned char k, int x, int y)
{
	Real32 ed;
	switch(k)
	{
	case 'q':
	case 27: 
		cleanup();
		exit(EXIT_SUCCESS);
		break;
	case 'e':
		ed = mgr->getEyeSeparation() * .9f;
		std::cout << "Eye distance: " << ed << '\n';
		mgr->setEyeSeparation(ed);
		break;
	case 'E':
		ed = mgr->getEyeSeparation() * 1.1f;
		std::cout << "Eye distance: " << ed << '\n';
		mgr->setEyeSeparation(ed);
		break;
	case 'h':
		cfg.setFollowHead(!cfg.getFollowHead());
		std::cout << "following head: " << std::boolalpha << cfg.getFollowHead() << '\n';
		break;
	case 'i':
		print_tracker();
		break;
	case 'r':
		reset_scene();
		break;
	case '4':
		physicsDebug(Vec3f(-.5f, 0, 0));
		break;
	case '6':
		physicsDebug(Vec3f(.5f, 0, 0));
		break;
	case '2':
		physicsDebug(Vec3f(0, -.5f, 0));
		break;
	case '8':
		physicsDebug(Vec3f(0, .5f, 0));
		break;
	case '7':
		physicsDebug(Vec3f(-.5f, .5f, 0));
		break;
	case '9':
		physicsDebug(Vec3f(.5f, .5f, 0));
		break;
	case '1':
		physicsDebug(Vec3f(-.5f, -.5f, 0));
		break;
	case '3':
		physicsDebug(Vec3f(.5f, -.5f, 0));
		break;

	case 'k':
		mgr->addYRotate(0.1f);
		break;
	case 'l':
		mgr->addYRotate(-0.1f);
		break;



	case '+':
		BallHitSpeedScale *= 2;
		std::cout << "Set BallHitSpeedScale to " << BallHitSpeedScale << std::endl;
		break;

	case '-':
		BallHitSpeedScale /= 2;
		std::cout << "Set BallHitSpeedScale to " << BallHitSpeedScale << std::endl;
		break;
	default:
		std::cout << "Key '" << k << "' ignored\n";
	}
}



void setupGLUT(int *argc, char *argv[])
{
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB  |GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow("OpenSG CSMDemo with VRPN API");
	glutDisplayFunc([]()
	{
		// black navigation window
		glClear(GL_COLOR_BUFFER_BIT);
		glutSwapBuffers();
	});
	glutReshapeFunc([](int w, int h)
	{
		mgr->resize(w, h);
		glutPostRedisplay();
	});
	glutKeyboardFunc(keyboard);
	glutIdleFunc([]()
	{
		check_tracker();
		const auto speed = 1.f;
		mgr->setUserTransform(head_position, head_orientation);
		//mgr->setTranslation(mgr->getTranslation() + speed * analog_values);

		mgr->addYRotate(-analog_values[0] * MOVEMENT_VALUE_SCALE);

		// need to rotate wand (in CAVE coords) by camera rotation to get world position

		Vec3f wand_pos_rot;

		Quaternion mgrQuart(Vec3f(0, 1, 0), mgr->getYRotate());
		mgrQuart.multVec(wand_position, wand_pos_rot);

		mgrQuart.multVec(wand_speed, wand_world_speed);

		wand_world_position = wand_pos_rot + mgr->getTranslation();
		wand_world_orientation = mgrQuart * wand_orientation;



		updatePhysics();


		commitChanges();
		mgr->redraw();
		// the changelist should be cleared - else things could be copied multiple times
		OSG::Thread::getCurrentChangeList()->clear();
	});
}

int main(int argc, char **argv)
{

#if WIN32
	OSG::preloadSharedObject("OSGFileIO");
	OSG::preloadSharedObject("OSGImageFileIO");
	system("pause");
#endif


	try
	{
		bool cfgIsSet = false;
		NodeRefPtr scene = nullptr;

		// ChangeList needs to be set for OpenSG 1.4
		ChangeList::setReadWriteDefault();
		osgInit(argc,argv);

		// evaluate intial params
		for(int a=1 ; a<argc ; ++a)
		{
			if( argv[a][0] == '-' )
			{
				if ( strcmp(argv[a],"-f") == 0 ) 
				{
					char* cfgFile = argv[a][2] ? &argv[a][2] : &argv[++a][0];
					if (!cfg.loadFile(cfgFile)) 
					{
						std::cout << "ERROR: could not load config file '" << cfgFile << "'\n";
						return EXIT_FAILURE;
					}
					cfgIsSet = true;
				}
			} else {
				std::cout << "Loading scene file '" << argv[a] << "'\n";
				scene = SceneFileHandler::the()->read(argv[a], NULL);
			}
		}

		// load the CAVE setup config file if it was not loaded already:
		if (!cfgIsSet) 
		{
			const char* const default_config_filename = "config/mono.csm";
			if (!cfg.loadFile(default_config_filename)) 
			{
				std::cout << "ERROR: could not load default config file '" << default_config_filename << "'\n";
				return EXIT_FAILURE;
			}
		}

		cfg.printConfig();

		setupGLUT(&argc, argv);

		InitTracker(cfg);

		MultiDisplayWindowRefPtr mwin = createAppWindow(cfg, cfg.getBroadcastaddress());

		if (!scene)
		{
			scene = buildScene();
			reset_scene();
		}

		commitChanges();

		mgr = new OSGCSM::CAVESceneManager(&cfg);
		mgr->setWindow(mwin );
		mgr->setRoot(scene);
		mgr->showAll();
		mgr->getWindow()->init();
		mgr->turnWandOff();
	}
	catch(const std::exception& e)
	{
		std::cout << "ERROR: " << e.what() << '\n';
		return EXIT_FAILURE;
	}

	glutMainLoop();
}
