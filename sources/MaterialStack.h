#include <OpenSG/OSGMaterialGroup.h>
#include <OpenSG/OSGImage.h>
#include <OpenSG/OSGSimpleTexturedMaterial.h>
#include <OpenSG/OSGSceneFileHandler.h>

OSG_USING_NAMESPACE

class MaterialStack {
public:
	static SimpleMaterialRecPtr Debug();
	static SimpleMaterialRecPtr Ball();
	static SimpleMaterialRecPtr MovRayGreen();
	static SimpleMaterialRecPtr MovRayRed();
	static SimpleTexturedMaterialRecPtr ConcreteWall();
	static SimpleTexturedMaterialRecPtr MetalWall();
	static SimpleTexturedMaterialRecPtr Ceiling();
	static SimpleTexturedMaterialRecPtr WinCube();
	static SimpleTexturedMaterialRecPtr Floor();

private:
	MaterialStack(void){}
	static SimpleMaterialRecPtr m_debug;
	static SimpleMaterialRecPtr m_ball;
	static SimpleMaterialRecPtr m_movray_green;
	static SimpleMaterialRecPtr m_movray_red;
	static SimpleTexturedMaterialRecPtr m_wall_con;
	static SimpleTexturedMaterialRecPtr m_wall_metal;
	static SimpleTexturedMaterialRecPtr m_ceiling;
	static SimpleTexturedMaterialRecPtr m_wincube;
	static SimpleTexturedMaterialRecPtr m_floor1;
	static SimpleTexturedMaterialRecPtr m_floor2;
	static SimpleTexturedMaterialRecPtr m_floor3;
};
