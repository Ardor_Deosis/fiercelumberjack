/*
 * Implemented by Daniel Frejek (ga82zuy)
 */

#include <OpenSG/OSGBaseTypes.h>
#include <OpenSG/OSGBoxVolume.h>

#include <vector>
#include <map>

OSG_USING_NAMESPACE

class AABBCollision
{
	public:
		explicit AABBCollision();
		~AABBCollision();

		enum CollisionAxis
		{
			CollisionAxisNone = 0,
			CollisionAxisXFwd = (1 << 0),
			CollisionAxisYFwd = (1 << 1),
			CollisionAxisZFwd = (1 << 2),
			CollisionAxisXBwd = (1 << 3),
			CollisionAxisYBwd = (1 << 4),
			CollisionAxisZBwd = (1 << 5),
			CollisionAxisX = CollisionAxisXFwd | CollisionAxisXBwd,
			CollisionAxisY = CollisionAxisYFwd | CollisionAxisYBwd,
			CollisionAxisZ = CollisionAxisZFwd | CollisionAxisZBwd
		};

		typedef std::map<std::string, void *> ColliderData;

		// Adds a new box to the collision system. After adding boxes finalize() must be called.
		void addBox(const BoxVolume &box, const ColliderData &data = ColliderData());

		// removes all boxes
		void clear();

		// must be called after adding the boxes (before the sphere / raycast)
		void finalize();

		// spherecast (actually this is a boxcast)
		// it returns all collided axes
		int spherecast(Real32 radius, Vec3f start, Vec3f dir, ColliderData **data = NULL);

		// raycast
		// returns the hit surface
		CollisionAxis raycast(Vec3f start, Vec3f end, Vec3f* hit, ColliderData **data = NULL);

	private:
		struct QuadEntry
		{
			Real32 sortedPos;
			Real32 firstAxisStart;
			Real32 firstAxisEnd;
			Real32 secondAxisStart;
			Real32 secondAxisEnd;

			size_t dataIndex;
			bool bwd;

			inline bool operator<(const QuadEntry &other) const { return sortedPos < other.sortedPos; }
			bool overlap(const QuadEntry &other) const;
		};

		std::vector<ColliderData> _colliderData;

		typedef std::vector<QuadEntry> SortedAxis;
		typedef SortedAxis::iterator SortedAxisIt;
		typedef std::pair<SortedAxisIt, SortedAxisIt> AxisRange;

		bool quadContainsQuadAtPos(const QuadEntry &q, const QuadEntry &startRect, const Vec2f &nonAxisMovement, Real32 dist) const;
		size_t firstMatchFwd(const SortedAxis &axis, const QuadEntry &startRect, const Vec2f &nonAxisMovement, Real32 start, Real32 length) const;
		size_t firstMatchBwd(const SortedAxis &axis, const QuadEntry &startRect, const Vec2f &nonAxisMovement, Real32 start, Real32 length) const;

		size_t firstMatch(CollisionAxis ax, const Vec3f &start, const Vec3f &dir, Real32 boxSize) const;

		Real32 relDistToPlane(const SortedAxis &axis, size_t index, Real32 start, Real32 length) const;

		SortedAxis _sortedX;
		SortedAxis _sortedY;
		SortedAxis _sortedZ;
};
