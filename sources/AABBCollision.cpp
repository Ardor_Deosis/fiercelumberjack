/*
* Implemented by Daniel Frejek (ga82zuy)
*/

#include "AABBCollision.h"

#include <stdint.h>
#include <limits.h>

#include <algorithm>

AABBCollision::AABBCollision()
{

}


AABBCollision::~AABBCollision()
{

}


void AABBCollision::addBox(const BoxVolume &box, const ColliderData &data)
{
	Real32 xmin; 
	Real32 ymin; 
	Real32 zmin;
	Real32 xmax;
	Real32 ymax; 
	Real32 zmax;

	box.getBounds(xmin, ymin, zmin, xmax, ymax, zmax);

	_colliderData.push_back(data);

	QuadEntry q;

	q.dataIndex = _colliderData.size() - 1;

	// x-
	q.bwd = false;
	q.sortedPos = xmin;
	q.firstAxisStart = ymin;
	q.firstAxisEnd = ymax;
	q.secondAxisStart = zmin;
	q.secondAxisEnd = zmax;
	_sortedX.push_back(q);

	// x+
	q.bwd = true;
	q.sortedPos = xmax;
	_sortedX.push_back(q);

	// y-
	q.bwd = false;
	q.sortedPos = ymin;
	q.firstAxisStart = xmin;
	q.firstAxisEnd = xmax;
	q.secondAxisStart = zmin;
	q.secondAxisEnd = zmax;
	_sortedY.push_back(q);

	// y+
	q.bwd = true;
	q.sortedPos = ymax;
	_sortedY.push_back(q);

	// z-
	q.bwd = false;
	q.sortedPos = zmin;
	q.firstAxisStart = xmin;
	q.firstAxisEnd = xmax;
	q.secondAxisStart = ymin;
	q.secondAxisEnd = ymax;
	_sortedZ.push_back(q);

	// z+
	q.bwd = true;
	q.sortedPos = zmax;
	_sortedZ.push_back(q);
}


void AABBCollision::clear()
{
	_sortedX.clear();
	_sortedY.clear();
	_sortedZ.clear();
}


void AABBCollision::finalize()
{
	std::sort(_sortedX.begin(), _sortedX.end());
	std::sort(_sortedY.begin(), _sortedY.end());
	std::sort(_sortedZ.begin(), _sortedZ.end());
}


int AABBCollision::spherecast(Real32 radius, Vec3f start, Vec3f dir, ColliderData **data)
{
	size_t colX = firstMatch(CollisionAxisX, start, dir, radius);
	size_t colY = firstMatch(CollisionAxisY, start, dir, radius);
	size_t colZ = firstMatch(CollisionAxisZ, start, dir, radius);

	size_t colIdx = 0;

	int res = 0;
	if (colX != -1)
	{
		res |= (dir[0] > 0) ? CollisionAxisXFwd : CollisionAxisXBwd;
		colIdx = _sortedX[colX].dataIndex;
	}

	if (colY != -1)
	{
		res |= (dir[1] > 0) ? CollisionAxisYFwd : CollisionAxisYBwd;
		colIdx = _sortedY[colY].dataIndex;
	}

	if (colZ != -1)
	{
		res |= (dir[2] > 0) ? CollisionAxisZFwd : CollisionAxisZBwd;
		colIdx = _sortedZ[colZ].dataIndex;
	}

	if ((res != 0) && (data != NULL))
	{
		(*data) = &(_colliderData[colIdx]);
	}

	return res;
}


AABBCollision::CollisionAxis AABBCollision::raycast(Vec3f start, Vec3f end, Vec3f* hit, ColliderData **data) {

	Vec3f dist = end - start;
	size_t colX = firstMatch(CollisionAxisX, start, dist, 0);
	size_t colY = firstMatch(CollisionAxisY, start, dist, 0);
	size_t colZ = firstMatch(CollisionAxisZ, start, dist, 0);


	size_t colIdx = 0;


	CollisionAxis minAxis = CollisionAxisNone;
	Real32 minT = 2;

	if (colX != -1)
	{
		Real32 t = relDistToPlane(_sortedX, colX, start.x(), dist.x());
		if (t < minT)
		{
			minAxis = (dist[0] > 0) ? CollisionAxisXFwd : CollisionAxisXBwd;
			minT = t;
			colIdx = _sortedX[colX].dataIndex;
		}
	}

	if (colY != -1)
	{
		Real32 t = relDistToPlane(_sortedY, colY, start.y(), dist.y());
		if (t < minT)
		{
			minAxis = (dist[1] > 0) ? CollisionAxisYFwd : CollisionAxisYBwd;
			minT = t;
			colIdx = _sortedY[colX].dataIndex;
		}
	}

	if (colZ != -1)
	{
		Real32 t = relDistToPlane(_sortedZ, colZ, start.z(), dist.z());
		if (t < minT)
		{
			minAxis = (dist[2] > 0) ? CollisionAxisZFwd : CollisionAxisZBwd;
			minT = t;
			colIdx = _sortedZ[colX].dataIndex;
		}
	}

	if (minT <= 1.0f)
	{
		// collision at time minT
		// -> point = start + dist * minT
		(*hit) = start + dist * minT;

		if (data != NULL)
		{
			(*data) = &(_colliderData[colIdx]);
		}

		return minAxis;
	}
	else
	{
		// no collision
		return CollisionAxisNone;
	}
}


bool AABBCollision::QuadEntry::overlap(const QuadEntry &other) const
{
	return !(firstAxisStart > other.firstAxisEnd ||
		secondAxisStart > other.secondAxisEnd ||
		firstAxisEnd < other.firstAxisStart ||
		secondAxisEnd < other.secondAxisStart);
}


bool AABBCollision::quadContainsQuadAtPos(const QuadEntry &q, const QuadEntry &startRect, const Vec2f &nonAxisMovement, Real32 dist) const
{
	QuadEntry quadAtDist;
	quadAtDist.firstAxisStart = startRect.firstAxisStart + nonAxisMovement.x() * dist;
	quadAtDist.secondAxisStart = startRect.secondAxisStart + nonAxisMovement.y() * dist;
	quadAtDist.firstAxisEnd = startRect.firstAxisEnd + nonAxisMovement.x() * dist;
	quadAtDist.secondAxisEnd = startRect.secondAxisEnd + nonAxisMovement.y() * dist;

	return q.overlap(quadAtDist);
}


size_t AABBCollision::firstMatchFwd(const SortedAxis &axis, const QuadEntry &startRect, const Vec2f &nonAxisMovement, Real32 start, Real32 length) const
{
	if (axis.empty())
	{
		return -1;
	}

	QuadEntry tmp;
	tmp.sortedPos = start;
	auto next = std::upper_bound(axis.begin(), axis.end(), tmp);

	while (next != axis.end())
	{
		if (next->sortedPos > start + length)
		{
			break;
		}

		Real32 dist = next->sortedPos - start;

		if ((!next->bwd) && quadContainsQuadAtPos(*next, startRect, nonAxisMovement, dist))
		{
			// overlap at collision point -> real collision
			// TODO / MAYBE: This is currently just a box / box collision (maybe we want sphere <-> box)

			return next - axis.begin();
		}

		next++;
	}

	return -1;
}


size_t AABBCollision::firstMatchBwd(const SortedAxis &axis, const QuadEntry &startRect, const Vec2f &nonAxisMovement, Real32 start, Real32 length) const
{
	if (axis.empty())
	{
		return -1;
	}

	QuadEntry tmp;
	tmp.sortedPos = start;
	auto next = std::lower_bound(axis.begin(), axis.end(), tmp);

	if (next == axis.end())
	{
		next = axis.end() - 1;
	}

	for (;; next--)
	{
		if (next->sortedPos > start)
		{
			if (next == axis.begin())
			{
				break;
			}
			continue;
		}

		if (next->sortedPos < start - length)
		{
			break;
		}

		Real32 dist = start - next->sortedPos;

		if (next->bwd && quadContainsQuadAtPos(*next, startRect, nonAxisMovement, dist))
		{
			// overlap at collision point -> real collision
			// TODO / MAYBE: This is currently just a box / box collision (maybe we want sphere <-> box)

			return next - axis.begin();
		}

		if (next == axis.begin())
		{
			break;
		}

	}

	return -1;
}


size_t AABBCollision::firstMatch(CollisionAxis ax, const Vec3f &start, const Vec3f &dir, Real32 boxSize) const
{
	size_t firstAx;
	size_t secondAx;
	size_t fwdAx;

	const SortedAxis *axis;

	switch (ax)
	{
	case CollisionAxisX:
		firstAx = 1;
		secondAx = 2;
		fwdAx = 0;
		axis = &_sortedX;
		break;

	case CollisionAxisY:
		firstAx = 0;
		secondAx = 2;
		fwdAx = 1;
		axis = &_sortedY;
		break;

	case CollisionAxisZ:
		firstAx = 0;
		secondAx = 1;
		fwdAx = 2;
		axis = &_sortedZ;
		break;

	default:
		return -1;
	}

	if (dir[fwdAx] == 0)
	{
		return -1;
	}

	Real32 dist = dir[fwdAx];
	bool bwd = false;
	if (dist < 0)
	{
		bwd = true;
		dist = -dist;
	}

	QuadEntry rect;
	rect.firstAxisStart = start[firstAx] - boxSize;
	rect.secondAxisStart = start[secondAx] - boxSize;
	rect.firstAxisEnd = start[firstAx] + boxSize;
	rect.secondAxisEnd = start[secondAx] + boxSize;

	Vec2f rectMovement(dir[firstAx] / dist, dir[secondAx] / dist);

	if (bwd)
	{
		return firstMatchBwd(*axis, rect, rectMovement, start[fwdAx] - boxSize, dist);
	}
	else
	{
		return firstMatchFwd(*axis, rect, rectMovement, start[fwdAx] + boxSize, dist);
	}
}


Real32 AABBCollision::relDistToPlane(const SortedAxis &axis, size_t index, Real32 start, Real32 length) const
{
	Real32 plane = axis[index].sortedPos;

	Real32 distance = plane - start;

	if (distance < 0)
	{
		distance = -distance;
	}

	if (length < 0)
	{
		length = - length;
	}

	return distance / length;
}