#include <OpenSG/OSGMaterialGroup.h>
#include <OpenSG/OSGImage.h>
#include <OpenSG/OSGSimpleTexturedMaterial.h>
#include <OpenSG/OSGSceneFileHandler.h>

#include "MaterialStack.h"


SimpleMaterialRecPtr MaterialStack::m_debug = NULL;
SimpleMaterialRecPtr MaterialStack::m_ball = NULL;
SimpleMaterialRecPtr MaterialStack::m_movray_green = NULL;
SimpleMaterialRecPtr MaterialStack::m_movray_red = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_wall_con = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_wall_metal = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_ceiling = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_wincube = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_floor1 = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_floor2 = NULL;
SimpleTexturedMaterialRecPtr MaterialStack::m_floor3 = NULL;



SimpleMaterialRecPtr MaterialStack::Debug () {
	if (!m_debug) {
		m_debug = SimpleMaterial::create();
		m_debug->setDiffuse(Color3f(0, 0, 0));
		m_debug->setAmbient(Color3f(0, 0, 0));
		m_debug->setEmission(Color3f(1, 0, 1));
	}
	return m_debug;
}

SimpleMaterialRecPtr MaterialStack::Ball () {
	if (!m_ball) {
		m_ball = SimpleMaterial::create();
		m_ball->setDiffuse(Color3f(1,0.8f,0));
		m_ball->setAmbient(Color3f(0.8f, 0.2f, 0.2f));
		m_ball->setShininess(1.0f);
	}
	return m_ball;
}

SimpleMaterialRecPtr MaterialStack::MovRayGreen () {
	if (!m_movray_green) {
		m_movray_green = SimpleMaterial::create();
		m_movray_green->setDiffuse(Color3f(0, 1, 0));
		m_movray_green->setAmbient(Color3f(0, 0.5f, 0));
		m_movray_green->setTransparency(0.7f);
	}
	return m_movray_green;
}

SimpleMaterialRecPtr MaterialStack::MovRayRed () {
	if (!m_movray_red) {
		m_movray_red = SimpleMaterial::create();
		m_movray_red->setDiffuse(Color3f(1, 0, 0));
		m_movray_red->setAmbient(Color3f(0.5f, 0, 0));
		m_movray_red->setTransparency(0.7f);
	}
	return m_movray_red;
}

SimpleTexturedMaterialRecPtr MaterialStack::ConcreteWall () {
	if (!m_wall_con) {
		ImageRecPtr tex = Image::create();
		tex->read("textures/ConcreteWall.jpg");
		m_wall_con = SimpleTexturedMaterial::create();
		m_wall_con->setImage(tex);
	}
	return m_wall_con;
}

SimpleTexturedMaterialRecPtr MaterialStack::MetalWall () {
	if (!m_wall_metal) {
		ImageRecPtr tex = Image::create();
		tex->read("textures/MetalWall.jpg");
		m_wall_metal = SimpleTexturedMaterial::create();
		m_wall_metal->setImage(tex);
	}
	return m_wall_metal;
}

SimpleTexturedMaterialRecPtr MaterialStack::Ceiling () {
	if (!m_ceiling) {
		ImageRecPtr tex = Image::create();
		tex->read("textures/Concrete4.jpg");
		m_ceiling = SimpleTexturedMaterial::create();
		m_ceiling->setImage(tex);
	}
	return m_ceiling;
}

SimpleTexturedMaterialRecPtr MaterialStack::WinCube () {
	if (!m_wincube) {
		ImageRecPtr tex = Image::create();
		tex->read("textures/goal.jpg");
		m_wincube = SimpleTexturedMaterial::create();
		m_wincube->setImage(tex);
	}
	return m_wincube;
}

SimpleTexturedMaterialRecPtr MaterialStack::Floor () {
	switch (rand() % 3 + 1) {
	case 1:
		if (!m_floor1) {
			ImageRecPtr tex = Image::create();
			tex->read("textures/Concrete1.jpg");
			tex->read("textures/wall.jpg");
			m_floor1 = SimpleTexturedMaterial::create();
			m_floor1->setImage(tex);
			m_floor1->setDiffuse(Color3f(1,0.8f,0));
			m_floor1->setAmbient(Color3f(0.8f, 0.2f, 0.2f));
		}
		return m_floor1;
		break;
	case 2:
		if (!m_floor2) {
			ImageRecPtr tex = Image::create();
			tex->read("textures/Concrete2.jpg");
			tex->read("textures/wall2.jpg");
			m_floor2 = SimpleTexturedMaterial::create();
			m_floor2->setImage(tex);
		}
		return m_floor2;
		break;
	default:
		if (!m_floor3) {
			ImageRecPtr tex = Image::create();
			tex->read("textures/Concrete3.jpg");
			tex->read("textures/wall3.jpg");
			m_floor3 = SimpleTexturedMaterial::create();
			m_floor3->setImage(tex);
		}
		return m_floor3;
		break;
	}
}

