/*
* Implemented by Daniel Frejek (ga82zuy)
*/

#include "AABBCollision.h"
#include <OpenSG/OSGGLUT.h>
#include <OpenSG/OSGComponentTransform.h>
#include <OSGCSM/OSGCAVESceneManager.h>

const unsigned int PHYSICS_TIMESTEP_LENGTH = 20; // ms

extern OSGCSM::CAVESceneManager *mgr;

AABBCollision cd;


float ballSize = 10.0f;
const float MinBallSpeed = 5.0f / PHYSICS_TIMESTEP_LENGTH; // 5 cm/s
const float BallIdleOffset = 130.0f;

float BallHitSpeedScale = 15000.0f;
extern ComponentTransformRecPtr ballTrans;
extern ComponentTransformRecPtr testBallTrans;
Vec3f ballSpeed;
bool ballIsIdle = false;
float ballIdleTime;
Vec3f ballIdleInitialPos;

bool ballGrabActive = false;
bool ballGrabbed = false;

Vec3f wand_speed;	// units per second

extern bool movementValid;
extern bool movementRayVisible;


void updateBallTrail();
void updateMovementRay();

extern Vec3f wand_world_position;
extern Vec3f wand_world_speed;

void reset_scene()
{
	ballGrabbed = false;
	movementValid = false;
	ballIsIdle = false;

	if (mgr)
	{
		// reset mgr pos
		mgr->setTranslation(Vec3f(0, 0, 0));
		mgr->setYRotate(0);
	}

	// reset ball
	ballTrans->setTranslation(Vec3f(100, 150, 100));
	ballSpeed = Vec3f(-.1, 0, 0);
}


void performPhysicStep () {
	// gravity
	const float gravity = 0.2f;
	const float dampingAir = 0.999f;
	const float dampingCollision = 0.75f;
	const float dampingCollisionOtherAxis = 0.9f;

	const float wandColliderRadius = 30.0f;

	Vec3f pos = ballTrans->getTranslation();

	// check wand-ball collision
	if (ballGrabActive && !ballGrabbed && ((pos - wand_world_position).squareLength() < ((wandColliderRadius + ballSize) * (wandColliderRadius + ballSize))))
	{
		ballGrabbed = true;
		ballIsIdle = false;

		std::cout << "grab ball" << std::endl;
	}
	else if (!ballGrabActive && ballGrabbed)
	{
		// release ball -> set ball speed to wand speed multiplied by hitSpeedFactor
		std::cout << "release ball" << std::endl;
		ballSpeed = -wand_world_speed * BallHitSpeedScale * PHYSICS_TIMESTEP_LENGTH / 1000;

		ballGrabbed = false;

	}

	if (ballGrabbed)
	{
		// set ball pos to wand pos

		// BUT first check, if the movement s valid!
		if (cd.spherecast(ballSize, pos, wand_world_position - pos))
		{
			// Nope, don't want to make this move, release the ball!
			ballGrabbed = false;
			return;
		}

		ballTrans->setTranslation(wand_world_position);
		return;
	}

	if (ballIsIdle)
	{
		// set y to offset

		ballIdleTime += PHYSICS_TIMESTEP_LENGTH / 1000.0f;

		pos = ballIdleInitialPos;
		pos[1] += BallIdleOffset - (cos(ballIdleTime * 2) / osgClamp<float>(1, ballIdleTime * 2, 5)) * BallIdleOffset;
		ballTrans->setTranslation(pos);
		return;
	}

	// apply gravity
	ballSpeed[1] -= gravity;

	AABBCollision::ColliderData *colData;
	int colFaces = cd.spherecast(ballSize, pos, ballSpeed, &colData);

	if (colFaces != 0)
	{
		// check win condition
		if (colData->find("win_trigger") != colData->end())
		{
			// win
			reset_scene();
			return;
		}

		if (colFaces & AABBCollision::CollisionAxisX)
		{
			ballSpeed[0] *= -dampingCollision;
			ballSpeed *= dampingCollisionOtherAxis;
		}

		if (colFaces & AABBCollision::CollisionAxisY)
		{
			ballSpeed[1] *= -dampingCollision;
			ballSpeed *= dampingCollisionOtherAxis;

			if ((colFaces & AABBCollision::CollisionAxisYBwd) != 0)
			{
				std::cout << "detected ground collision, speed=" << ballSpeed.squareLength() << std::endl;
			}


			if ((colFaces & AABBCollision::CollisionAxisYBwd) != 0 && ballSpeed.squareLength() < MinBallSpeed * MinBallSpeed)
			{
				// -y collision && speed < limit
				ballIsIdle = true;
				ballIdleTime = 0;
				ballSpeed = Vec3f();

				std::cout << "set ball idle mode" << std::endl;

				ballIdleInitialPos = pos;
			}
		}

		if (colFaces & AABBCollision::CollisionAxisZ)
		{
			ballSpeed[2] *= -dampingCollision;
			ballSpeed *= dampingCollisionOtherAxis;
		}
	}
	else
	{
		pos += ballSpeed;
		ballTrans->setTranslation(pos);
	}

	ballSpeed *= dampingAir;
}

void updatePhysics()
{
	//return;

	static int tLastFrame = glutGet(GLUT_ELAPSED_TIME);  // milliseconds!
	int tCurrentFrame = glutGet(GLUT_ELAPSED_TIME);  // milliseconds!

	while (tLastFrame < tCurrentFrame) {
		tLastFrame += PHYSICS_TIMESTEP_LENGTH;
		performPhysicStep();
		updateBallTrail();
		Vec3f p = Vec3f(0.0f, 0.0f, 0.0f);
		Vec3f v = Vec3f(1.0f, 1.0f, 1.0f);
		if(movementRayVisible) {
			updateMovementRay();
		}
	}

	testBallTrans->setTranslation(wand_world_position);
}

void physicsDebug(Vec3f m)
{
	ballSpeed = m;

	Vec3f pos = ballTrans->getTranslation();

	int colFaces = cd.spherecast(ballSize, pos, ballSpeed);

	if (colFaces != 0)
	{
		std::cout << "Col: " << colFaces << '\n';

		if (colFaces & AABBCollision::CollisionAxisX)
		{
			ballSpeed[0] *= -1.0f;
		}

		if (colFaces & AABBCollision::CollisionAxisY)
		{
			ballSpeed[1] *= -1.0f;
		}

		if (colFaces & AABBCollision::CollisionAxisZ)
		{
			ballSpeed[2] *= -1.0f;
		}
	}
	else
	{
		pos += ballSpeed;
		ballTrans->setTranslation(pos);
	}
}

static const float wand_speed_filter_gain = 0.1f;
void update_wand_speed(Vec3f newPos)
{
	static auto wand_position_last = Vec3f();

	static int tLast = glutGet(GLUT_ELAPSED_TIME);  // milliseconds!
	int tCurrent = glutGet(GLUT_ELAPSED_TIME);  // milliseconds!

	Vec3f speed_cur = (newPos - wand_position_last) * (tLast - tCurrent) / 1000;

	// this is callback rate dependent
	wand_speed = wand_speed * (1 - wand_speed_filter_gain) + wand_speed_filter_gain * speed_cur;

	float raw_speed = speed_cur.length();
	float speed_flitered = wand_speed.length();

	std::cout << raw_speed << "," << speed_flitered << std::endl;

	wand_position_last = newPos;
	tLast = tCurrent;
}